<?php

namespace App\Http\Controllers;

use App\Models\Crud;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    public function view()
    {
        $products = Crud::latest()->paginate(5);

        return view('products.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function new()
    {
        return view('products.create');
    }

    public function saveProduct(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);

        $crud = new Crud;
        $crud->name = $request->name;
        $crud->description = $request->description;
        $crud->price = $request->price;
        $crud->save();

        return redirect()->route('products.get.view')
            ->with('success', 'Product created successfully.');
    }
    public function showProduct(Crud $product)
    {
        return view('products.show', compact('product'));
    }

    public function editProduct(Crud $product)
    {
        return view('products.edit', compact('product'));
    }

    public function updateProduct(Request $request, Crud $product)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);
        $product->update($request->all());

        return redirect()->route('products.get.view')
            ->with('success', 'Product updated successfully');
    }

    public function deleteProduct(Crud $product)
    {
        $product->delete();

        return redirect()->route('products.get.view')
            ->with('success', 'Product deleted successfully');
    }
}
